Dado("que esteja no site do Test Adress") do
  visit 'http://a.testaddressbook.com/sign_in'
  page.current_url
  puts page.title
end

Quando("clico em sign in") do
  @login.clicar_botao_entrar
  sleep 2
end

Então("preencho as informações necessarias para criar um usuario") do
  @login.preencher_info
end


Dado("que esteja logado no site do Test Adress") do
  visit 'http://a.testaddressbook.com/sign_in'
  @login = LoginPage.new
  @login.clicar_botao_entrar
  sleep 2
  @login.preencher_info
end

Quando("clico em addresses") do
  @endereco.clicarAdress
end

Então("preencho as informações necessarias para criar um endereço") do
  @endereco.preencherFormulario
end
class EnderecoPage < SitePrism::Page
  set_url 'http://a.testaddressbook.com/sign_in'

  element :botao_adress, "nav-item nav-link"
  element :botao_new_adress, "row justify-content-center"
  element :campo_first_name, id:"address_first_name"
  element :campo_last_name, id:"address_last_name"
  element :campo_adress, id:"address_street_address"
  element :campo_city, id:"address_city"
  element :campo_state, id:"address_state"
  element :campo_cep, id:"address_zip_code"
  element :campo_radio, id:"address_country_us"
  element :campo_data_aniversario, id:"address_birthday"
  element :campo_color, id:"address_color"
  element :campo_age, id:"address_age"
  element :campo_telefone, id:"address_phone"
  element :campo_dance, id:"address_interest_dance"
  element :campo_observacao, id:"address_note"

  def clicarAdress
    all(:css, ".nav-item.nav-link")[1].click
    find(".row.justify-content-center").click
  end


  def preencherFormulario
    sleep 2
    campo_first_name.send_keys "Joaozinho"
    sleep 2
    campo_last_name.send_keys "Silva"
    sleep 2
    campo_adress.send_keys "Rua Antonio Neto Caldeira 449"
    sleep 2
    campo_city.send_keys "São Paulo"
    sleep 2
    campo_state.click
    sleep 2
    campo_cep.send_keys "08765490"
    sleep 2
    campo_radio.click
    sleep 2
    campo_data_aniversario.send_keys "18091987"
    sleep 2
    campo_age.send_keys "37"
    sleep 2
    campo_telefone.send_keys "119965377648"
    sleep 2
    campo_dance.click
    sleep 2
    campo_observacao.send_keys "Teste 123"
    sleep 2
    all(:css, ".btn.btn-primary")[0].click
    sleep 5
    all(:css, ".nav-item.nav-link.active")[0].click
    sleep 5
    page.has_title? "Welcome to Address Book"
  end

end


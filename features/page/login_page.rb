class LoginPage < SitePrism::Page
  set_url 'http://a.testaddressbook.com/sign_in'

  element :botao_signin, :id, "sign-in"
  element :campo_email, :id, "session_email"
  element :campo_senha, :id, "session_password"
  element :botao_entrar, ".btn btn-primary"



  def clicar_botao_entrar
    botao_signin.click
  end

  def preencher_info
    campo_email.send_keys "teste@gmail.com"
    sleep 3
    campo_senha.send_keys "teste123"
    sleep 4
    find(".btn.btn-primary").click
    sleep 3
  end

end

